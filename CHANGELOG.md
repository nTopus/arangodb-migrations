# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0]

### Added
  - Feature generate migration file
  - Feature execute single migration (up/down) from version
  - Feature execute all migrations available

