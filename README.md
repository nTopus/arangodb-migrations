Arangodb-migrations
============================

The ArangoDb Migration library provides managed migration for ArangoDb Collections and Documents.

Configuration
------------------------

The configuration file needs to define: Arango connection configuration and Migration configuration.

This file is required.

Example Arango connection configuration "configuration.yml"

```yaml
---  
configuration:
 migrations_namespace: ArangoMigrationTest
 database: dummy_db
 collection_name: arango_migration_versions_test
 migrations_directory: migrations_arango
db-configuration:
 database: dummy_db
 host: tcp://arango.svc
 port: 8529
 user: root
 password: dummy_password
 options:
  reconnect: 1
```
> **Notes:**
> You need setting a valide migrations_directory 
> The database needs to be created before

Register Command
------------------------
This is an example how to register a console application in the directory

```php
#!/usr/bin/env php
require '../../vendor/autoload.php';

error_reporting(E_ALL & ~E_NOTICE);

use ArangoMigration\Application;

$application = new Application();
$application->registerCommands()
    ->run();
```

Execute Console Command
------------------------

```bash
> cd app/arango-migrations/
> ./console
Console Tool

Usage:
  [options] command [arguments]

Options:
  -h, --help            Display this help message
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi            Force ANSI output
      --no-ansi         Disable ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug


Available commands:
  help                          Displays help for a command
  list                          Lists commands
arangodb
arangodb:migrations:execute   Execute a single migration version up or down manually.
  arangodb:migrations:generate  Generate a blank migration class.
  arangodb:migrations:migrate   Execute a migration to a specified version or the latest available version.
```

Features - Generate a new Migration
------------------------
To generate a new migrate use the command
```bash
> ./console arangodb:migrations:generate --configuration=config/config.yaml
Generated new migration class to "migration_arango/Version20200311005355.php"
```

Features - Migrate a Migration
------------------------
To execute a single migrate use the command
```bash
> ./console.php arangodb:migration:execute --configuration=src/configuration.yaml 20200313212043
++ migrating 20200313212043
migrated (0.01)s
```
> **Notes:**
> You can use setting a direction to migrate (up, down)

Features - Migrate all Migrations
---------------------
To execute all available migrations (up/down) use the command
```bash
> ./console.php arangodb:migration:migrate --configuration=src/configuration.yaml
migrated (0.01)s
Migrating up to 20200313212043 from 0
++ migrating 20200310200614
migrated (0.01)s
## finished in (0.01)s
1 Migrations executed
```