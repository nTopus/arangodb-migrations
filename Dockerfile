#####
# See: https://hub.docker.com/_/php/
######

FROM php:7.2-fpm

######
# You can install php extensions using docker-php-ext-install
######

RUN apt-get update && apt-get install -y \
  supervisor \
  git \
  sudo \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  zlib1g-dev \
  libicu-dev

RUN docker-php-ext-install -j$(nproc) iconv zip \
  && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-install -j$(nproc) gd

RUN docker-php-ext-configure intl \
  && docker-php-ext-install -j$(nproc) \
    intl \
    opcache \
    pdo \
    pdo_mysql \
    sockets \
    bcmath

RUN curl -sS https://getcomposer.org/installer | php
RUN ln -s /var/www/html/composer.phar /usr/bin/composer

# Criando e configurando usuário docker
RUN useradd -m docker && echo "docker:docker" | chpasswd \
  && adduser docker sudo \
  && adduser docker root \
  && adduser docker www-data

#enable xterm, Alias and default folder
RUN echo "export TERM=xterm" >> /etc/bash.bashrc \
    && echo "alias _composer='php /usr/bin/composer'" >> /etc/bash.bashrc \
    && echo "alias _composerupdate='php /usr/bin/composer update'" >> /etc/bash.bashrc \
    && echo "alias _composerdump='sudo -H -u docker php /usr/bin/composer dumpautoload'" >> /etc/bash.bashrc \
    && echo "alias _testrun='vendor/bin/phpunit'" >> /etc/bash.bashrc

WORKDIR /opt/app

COPY ./src /opt/app


ENTRYPOINT ["php-fpm"]