<?php


namespace ArangoMigration;


use ArangoMigration\Command\ExecuteAllMigrationCommand;
use ArangoMigration\Command\ExecuteMigrationCommand;
use ArangoMigration\Command\GenerateMigrationCommand;
use Symfony\Component\Console\Application as SymfonyApplication;

class Application
{
    private $application;

    /**
     * Application constructor.
     * @param $application
     */
    public function __construct()
    {
        $this->application = new SymfonyApplication();
    }

    final public function registerCommands() {
        $this->application->addCommands(array(
            new GenerateMigrationCommand(),
            new ExecuteMigrationCommand(),
            new ExecuteAllMigrationCommand()
        ));
        return $this;
    }

    /**
     * @throws \Exception
     */
    final public function run() {
        $this->application->run();
    }

}