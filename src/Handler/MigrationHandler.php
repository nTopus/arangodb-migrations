<?php


namespace ArangoMigration\Handler;


use ArangoDBClient\ClientException;
use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Connection;
use ArangoDBClient\Cursor;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\Exception;
use ArangoDBClient\Statement;
use ArangoMigration\Configuration\Configuration;
use ArangoMigration\Migration\GetterSetterMagicAttributes;
use ArangoMigration\Migration\Migration;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationHandler
{
    use GetterSetterMagicAttributes;

    const UP = 'up';
    const DOWN = 'down';
    const VERSION = 'v';
    const TIME = 't';

    /** @var Migration[] */
    public $migrations;

    /** @var string */
    public $migrationCollectionName;

    /** @var CollectionHandler */
    public $collectionHandler;

    /** @var DocumentHandler */
    public $documentHandler;

    /** @var Connection */
    public $connection;

    /**
     * MigrationHandler constructor.
     * @param Configuration $configuration
     * @throws Exception
     * @throws \ArangoDBClient\ClientException
     * @throws \Exception
     */
    public function __construct(Configuration $configuration)
    {
        $this->migrations = $this->initializeMigrations(
            $configuration->migrationDirectory,
            $configuration->migrationsNamespace
        );
        $this->connection = new Connection($configuration->dbSettings);
        $this->collectionHandler = new CollectionHandler($this->connection);
        $this->documentHandler = new DocumentHandler($this->connection);
        $this->migrationCollectionName = $configuration->migrationCollectionName;
        $this->createMigrationCollection();
    }

    /**
     * @param string $path
     * @param string $namespace
     * @return array
     * @throws \Exception
     */
    protected function initializeMigrations(string $path, string $namespace): array
    {
        $path = realpath($path);
        $path = rtrim($path, "/");
        $files = glob($path . "/Version*.php");
        $migrations = [];
        $errors = "";
        foreach ($files as $file) {
            $basename = basename($file);
            $ok = preg_match("/(?<className>(^Version)(?<version>\d*))\.php$/", $basename, $matches);
            if (!$ok) {
                $errors .= " `$basename`";
                continue;
            }
            $migrationClass = '\\' . $namespace . '\\' . $matches["className"];
            $migration = new Migration($matches["version"], $migrationClass);
            $migrations[$migration->version] = $migration;
        }
        if (!empty($errors)) {
            throw new \UnexpectedValueException("Migrate failed (Reason: Fix file name -$errors)");
        }
        return $migrations;
    }

    public function getLastVersion(): string
    {
        $versions = array_keys($this->migrations);
        $latest = end($versions);
        return $latest !== false ? (string)$latest : '0';
    }

    /**
     * @throws Exception
     */
    public function getCurrentMigrationVersion(): int
    {
        $version = [];
        foreach ($this->migrations as $migration) {
            $version[] = $migration->version;
        }
        $statement = new Statement($this->connection, [
                'query' => "FOR m in @@collection FILTER m.v IN @versionList SORT m.v DESC LIMIT 1 RETURN m",
                'bindVars' => [
                    '@collection' => $this->migrationCollectionName,
                    'versionList' => $version
                ]
            ]
        );
        $cursor = $statement->execute();
        if ($cursor->getCount() == 0) {
            return 0;
        }
        /** @var Document $document */
        $document = $cursor->getAll()[0];
        return $document->get(self::VERSION);
    }

    /**
     * @param int $version
     * @return Migration
     * @throws \Exception
     */
    public function getMigrationByVersion(int $version): Migration
    {
        if (!isset($this->migrations[$version])) {
            throw new \Exception("Impossible execute migration {$version}.");
        }
        return $this->migrations[$version];
    }

    /**
     * @throws Exception
     * @throws \ArangoDBClient\ClientException
     */
    public function createMigrationCollection()
    {
        if (!$this->collectionHandler->has($this->migrationCollectionName)) {
            $collection = new Collection($this->migrationCollectionName);
            $this->collectionHandler->create($collection);
            $this->collectionHandler->createIndex($collection, ["type"=>"hash", "fields"=> self::VERSION, "unique" => true]);
        }
    }

    /**
     * @throws Exception
     * @throws \ArangoDBClient\ClientException
     */
    public function getMigratedVersion(): array
    {
        $cursor = $this->collectionHandler->all($this->migrationCollectionName);
        $version = [];
        /** @var Document $item */
        foreach ($cursor->getAll() as $item) {
            $version[] = $item->get(self::VERSION);
        }
        return $version;
    }

    public function getMigrationVersionAvailable(): array
    {
        $version = [];
        foreach ($this->migrations as $migration) {
            $version[] = $migration->version;
        }
        return $version;
    }

    /**
     * @param int $version
     * @return Cursor
     * @throws Exception
     */
    public function getMigrationDocumentByVersion(int $version): Cursor
    {
        return $this->collectionHandler->byExample(
            $this->migrationCollectionName,
            [self::VERSION => $version]
        );
    }

    /**
     * @param $version
     * @throws \ArangoDBClient\ClientException
     * @throws \ArangoDBClient\Exception
     */
    public function markMigrated(int $version)
    {
        $document = new Document();
        $document->set(self::VERSION, $version);
        $document->set(self::TIME, date('c'));
        $this->documentHandler->insert($this->migrationCollectionName, $document);
    }

    /**
     * @param int $version
     * @throws \ArangoDBClient\Exception
     */
    public function unmarkMigrated(int $version)
    {
        $cursor = $this->getMigrationDocumentByVersion($version);
        if ($cursor->getCount() == 0) {
            throw new Exception('Impossible execute migration');
        }
        /** @var Document $document */
        $document = $cursor->getAll()[0];
        $this->documentHandler->removeById($this->migrationCollectionName, $document->getId());
    }

    /**
     * @param $direction
     * @param $version
     * @return array
     * @throws Exception
     * @throws \ArangoDBClient\ClientException
     */
    public function getMigrationsAvailableToExecute(string $direction, int $version): array
    {
        $allMigrations = $this->migrations;
        if ($direction === self::DOWN && count($this->migrations) > 0) {
            $versions = array_reverse(array_keys($this->migrations));
            $classes = array_reverse(array_values($this->migrations));
            $allMigrations = array_combine($versions, $classes);
        }
        $migrationAvailable = [];
        $migrated = $this->getMigratedVersion();
        foreach ($allMigrations as $migration) {
            if ($this->shouldExecuteMigration($direction, $migration, $version, $migrated)) {
                $migrationAvailable[$migration->version] = $migration;
            }
        }
        return $migrationAvailable;
    }

    private function shouldExecuteMigration(string $direction, Migration $migration, int $version, array $migrated): bool
    {
        if ($direction === self::UP){
            return !in_array($migration->version, $migrated) && ($migration->version <= $version);
        }
        return in_array($migration->version, $migrated) && ($migration->version >= $version);

    }

    /**
     * @param OutputInterface $output
     * @param string|null $version
     * @param string $direction
     * @return int
     * @throws ClientException
     * @throws Exception
     */
    public function execute(OutputInterface $output, string $version = null, string $direction = ''): int
    {
        try {
            $migration = $this->getMigrationByVersion($version);
            $start = microtime(true);
            $message = ($direction === self::UP) ? "++ " :"-- ";
            $message .= "migrating {$version}";
            $name = $migration->className;
            $migrationClass = new $name();
            $migrationClass->$direction($this->connection, $this->collectionHandler, $this->documentHandler);
            $this->setMigrationMarking($version, $direction);
            $end = microtime(true);
            $time = round($end - $start, 4);
            $finalTime = number_format($time, 3);
            $output->writeln($message);
            $output->write("migrated ({$finalTime})s\n");
        } catch (\ArangoDBClient\ClientException $e) {
            $this->setMigrationMarking($version, $direction);
            $output->writeln("<error>Migration {$version} failed (Error: {$e->getMessage()})</error>");
            return 1;
        } catch (\Throwable $e) {
            $output->writeln("<error>Migration {$version} skipped (Reason: {$e->getMessage()})</error>");
            return 1;
        }
        return 0;
    }

    /**
     * @param $version
     * @param string $direction
     * @throws Exception
     * @throws \ArangoDBClient\ClientException
     */
    protected function setMigrationMarking(string $version, string $direction)
    {
        ($direction === self::UP) ? $this->markMigrated($version) : $this->unmarkMigrated($version);
    }
}