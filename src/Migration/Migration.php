<?php


namespace ArangoMigration\Migration;


class Migration
{
    use GetterSetterMagicAttributes;

    /** @var int */
    public $version;

    /** @var string */
    public $className;

    /** @var int */
    public $state;

    /**
     * Migration constructor.
     * @param int $version
     * @param string $className
     */
    public function __construct(int $version, string $className)
    {
        $this->version = $version;
        $this->className = $className;
    }

}