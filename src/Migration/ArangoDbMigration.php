<?php


namespace ArangoMigration\Migration;


use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Connection;
use ArangoDBClient\DocumentHandler;

interface ArangoDbMigration
{
    public function up(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler);
    public function down(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler);
}