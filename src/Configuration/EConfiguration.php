<?php


namespace ArangoMigration\Configuration;


abstract class EConfiguration
{
    const DB_CONFIGURATION = 'db-configuration';
    const CONFIGURATION = 'configuration';
    const PASSWORD = 'password';
    const USER = 'user';
    const DATABASE = 'database';
    const HOST = 'host';
    const PORT = 'port';
    const OPTIONS = 'options';
    const RECONNECT = 'reconnect';
    const COLLECTION_NAME = 'collection_name';
    const MIGRATIONS_NAMESPACE = 'migrations_namespace';
    const MIGRATIONS_DIRECTORY = 'migrations_directory';
}