<?php


namespace ArangoMigration\Configuration;

use ArangoDBClient\ConnectionOptions as ArangoConnectionOptions;
use ArangoMigration\Migration\GetterSetterMagicAttributes;
use Rakit\Validation\Validation;
use Symfony\Component\Yaml\Yaml;
use Rakit\Validation\Validator;

class Configuration
{
    use GetterSetterMagicAttributes;
    const REQUIRED = 'required';

    /** @var string */
    public $migrationsNamespace;

    /** @var string */
    public $migrationCollectionName;

    /** @var string */
    public $migrationDatabase;

    /** @var string */
    public $migrationDirectory;

    /** @var array */
    public $dbSettings = [];

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addSettings(string $key, $value)
    {
        $this->dbSettings[$key] = $value;
    }

    /**
     * Configuration constructor.
     * @param string $configurationFile
     * @throws \TheSeer\Tokenizer\Exception
     */
    public function __construct(string $configurationFile)
    {
        $path = getcwd() . '/' . $configurationFile;
        if (!file_exists($path)) {
            throw new \InvalidArgumentException('Configuration file not exist.');
        }
        $file = Yaml::parse(file_get_contents($path));
        if (empty($file)) {
            throw new \InvalidArgumentException('Configuration file not setting.');
        }
        $this->parseDbConfiguration($file);
        $this->parseMigrationConfiguration($file, $path);
    }

    /**
     * @param array $file
     */
    protected function parseDbConfiguration(array $file)
    {
        if (!isset($file[EConfiguration::DB_CONFIGURATION])) {
            throw new \InvalidArgumentException("The db-configuration setting does not exist.");
        }
        $parsed = $file[EConfiguration::DB_CONFIGURATION];
        $validation = $this->validateDbConfigurationSettings($parsed);
        if ($validation->fails()) {
            throw new \InvalidArgumentException("Invalid db-configuration setting (Reason: " . self::format($validation->errors()->toArray()) . ")");
        }
        if (isset($parsed[EConfiguration::PASSWORD])) {
            $this->addSettings(ArangoConnectionOptions::OPTION_AUTH_PASSWD, $parsed[EConfiguration::PASSWORD]);
        }
        if (isset($parsed[EConfiguration::USER])) {
            $this->addSettings(ArangoConnectionOptions::OPTION_AUTH_USER, $parsed[EConfiguration::USER]);
        }
        if (isset($parsed[EConfiguration::DATABASE])) {
            $this->addSettings(ArangoConnectionOptions::OPTION_DATABASE, $parsed[EConfiguration::DATABASE]);
        }
        if (isset($parsed[EConfiguration::HOST])) {
            $this->addSettings(ArangoConnectionOptions::OPTION_ENDPOINT, $parsed[EConfiguration::HOST]);
        }
        if (isset($parsed[EConfiguration::PORT])) {
            $this->addSettings(ArangoConnectionOptions::OPTION_ENDPOINT, $this->dbSettings[ArangoConnectionOptions::OPTION_ENDPOINT] . ':' . $parsed[EConfiguration::PORT]);
        }
        if (isset($parsed[EConfiguration::OPTIONS][EConfiguration::RECONNECT])) {
            $value = $parsed[EConfiguration::OPTIONS][EConfiguration::RECONNECT];
            $this->addSettings(ArangoConnectionOptions::OPTION_RECONNECT, (bool)$value);
        }
    }

    /**
     * @param array $fileContent
     * @param string $path
     */
    protected function parseMigrationConfiguration(array $fileContent, string $path)
    {
        if (!isset($fileContent[EConfiguration::CONFIGURATION])) {
            throw new \InvalidArgumentException("The configuration setting does not exist.");
        }
        $parsed = $fileContent[EConfiguration::CONFIGURATION];
        $validation = $this->validateConfigurationSettings($parsed);
        if ($validation->fails()) {
            throw new \InvalidArgumentException("Invalid configuration setting (Reason: " . self::format($validation->errors()->toArray()) . ")");
        }
        if (isset($parsed[EConfiguration::DATABASE])) {
            $this->migrationDatabase = $parsed[EConfiguration::DATABASE];
        }
        if (isset($parsed[EConfiguration::COLLECTION_NAME])) {
            $this->migrationCollectionName = $parsed[EConfiguration::COLLECTION_NAME];
        }
        if (isset($parsed[EConfiguration::MIGRATIONS_NAMESPACE])) {
            $this->migrationsNamespace = $parsed[EConfiguration::MIGRATIONS_NAMESPACE];
        }
        if (isset($parsed[EConfiguration::MIGRATIONS_DIRECTORY])) {
            $migrationsDirectory = $this->getDirectoryRelativeToFile($path, $parsed[EConfiguration::MIGRATIONS_DIRECTORY]);
            $this->migrationDirectory = $migrationsDirectory;
        }
    }

    protected function getDirectoryRelativeToFile(string $file, string $input): string
    {
        $path = realpath(dirname($file) . '/' . $input);
        if ($path !== false) {
            return $path;
        }
        return $input;
    }

    /**
     * @param array $parsed
     * @return Validation
     */
    protected function validateDbConfigurationSettings(array $parsed): Validation
    {
        $rules = [
            EConfiguration::PASSWORD => self::REQUIRED,
            EConfiguration::USER => self::REQUIRED,
            EConfiguration::DATABASE => self::REQUIRED,
            EConfiguration::HOST => self::REQUIRED,
            EConfiguration::PORT => self::REQUIRED
        ];
        $validator = new Validator;
        return $validator->validate($parsed, $rules);
    }

    /**
     * @param array $parsed
     * @return Validation
     */
    protected function validateConfigurationSettings(array $parsed): Validation
    {
        $rules = [
            EConfiguration::DATABASE => self::REQUIRED,
            EConfiguration::COLLECTION_NAME => self::REQUIRED,
            EConfiguration::MIGRATIONS_NAMESPACE => self::REQUIRED,
            EConfiguration::MIGRATIONS_DIRECTORY => self::REQUIRED,
        ];
        $validator = new Validator;
        return $validator->validate($parsed, $rules);
    }

    public static function format(array $errors): string
    {
        $localError = "";
        foreach ($errors as $attr => $ruleErrors) {
            foreach ($ruleErrors as $error) {
                $localError .= (!empty($localError)) ? ", " . $error :$error;
            }
        }
        return $localError;
    }
}