<?php


namespace ArangoMigration\Command;


use ArangoMigration\Configuration\Configuration;
use ArangoMigration\Configuration\EConfiguration;
use ArangoMigration\Migration\GetterSetterMagicAttributes;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BaseCommand extends SymfonyCommand
{

    use GetterSetterMagicAttributes;

    /** @var Configuration */
    public $configuration;

    protected function configure()
    {
        $this->addOption(EConfiguration::CONFIGURATION, null, InputOption::VALUE_REQUIRED, 'The path to migration controller file.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \TheSeer\Tokenizer\Exception
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        if (empty($input->getOption(EConfiguration::CONFIGURATION))) {
            throw new \InvalidArgumentException("The configuration option does not exist.");
        }
        $this->configuration = new Configuration($input->getOption(EConfiguration::CONFIGURATION));
    }

}