<?php


namespace ArangoMigration\Command;


use ArangoDBClient\ClientException;
use ArangoMigration\Handler\MigrationHandler;
use ArangoMigration\Migration\GetterSetterMagicAttributes;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HandlerCommand extends BaseCommand
{

    const VERSION = "version";

    const DIRECTION = "direction";

    const UP = "up";

    const DOWN = "down";

    use GetterSetterMagicAttributes;

    /** @var MigrationHandler */
    public $migrationHandler;

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws ClientException
     * @throws \ArangoDBClient\Exception
     * @throws \Exception
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->migrationHandler = new MigrationHandler($this->configuration);
    }
}