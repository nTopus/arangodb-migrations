<?php


namespace ArangoMigration\Command;


use ArangoMigration\Migration\Migration;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ExecuteAllMigrationCommand extends HandlerCommand
{
    protected static $defaultName = 'arangodb:migrations:migrate';

    protected function configure()
    {
        $this->setName($this->getName());
        $this->setDescription('Execute a migration to a specified version or the latest available version.');
        $this->addArgument(self::VERSION, InputArgument::OPTIONAL, 'The version to migrate to.', null);
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \ArangoDBClient\ClientException
     * @throws \ArangoDBClient\Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $version = $input->getArgument(self::VERSION);
        $executedVersions = $this->migrationHandler->getMigratedVersion();
        $availableVersions = $this->migrationHandler->getMigrationVersionAvailable();
        $migrationsAvailable = array_diff($executedVersions, $availableVersions);
        if ($migrationsAvailable) {
            $count = count($migrationsAvailable);
            $output->writeln("WARNING! You have {$count} previously executed migrations in the database that are not registered migrations.");
            foreach ($migrationsAvailable as $executedUnavailableVersion) {
                $output->writeln($executedUnavailableVersion);
            }
            if ($input->isInteractive()) {
                $helper = $this->getHelper('question');
                $question = new ConfirmationQuestion('Continue with this action? (y/n)', false);
                $confirmation = $helper->ask($input, $output, $question);
                if (!$confirmation) {
                    $output->writeln('Migration cancelled!');
                    return 1;
                }
            }
        }
        return $this->migrate($output, $version);
    }

    /**
     * @param OutputInterface $output
     * @param int|null $version
     * @return int|void
     * @throws \ArangoDBClient\ClientException
     * @throws \ArangoDBClient\Exception
     */
    private function migrate(OutputInterface $output, int $version = null): int
    {
        $time = 0;
        $start = microtime(true);
        if ($version === null) {
            $version = $this->migrationHandler->getLastVersion();
        }
        $lastVersionExecuted = $this->migrationHandler->getCurrentMigrationVersion();
        $migrations = $this->migrationHandler->migrations;
        $direction = $lastVersionExecuted >= $version ? self::DOWN : self::UP;
        $migrationsToExecute = $this->migrationHandler->getMigrationsAvailableToExecute($direction, $version);
        if (empty($migrationsToExecute)) {
            $output->writeln("<info>Could not find any migrations to execute.</info>");
            return 0;
        }
        if (!isset($migrations[$version])) {
            $output->writeln("<erro>Version unknown version {$version}</erro>");
            return 1;
        }
        if ($lastVersionExecuted === $version && empty($migrationsToExecute)) {
            $output->writeln("<info>Could not find any migrations to execute.</info>");
        }
        $output->writeln("<info>Migrating {$direction} to {$version} from {$lastVersionExecuted}</info>\n");
        /** @var Migration $migration */
        foreach ($migrationsToExecute as $migration) {
            if ($this->migrationHandler->execute($output, $migration->version, $direction) === 1) {
                return 1;
            }
            $end = microtime(true);
            $time += round($end - $start, 4);
        }
        $count = count($migrationsToExecute);
        $finalTime = number_format($time, 3);
        $output->writeln("## finished in ({$finalTime})s");
        $output->writeln("<info>{$count} Migrations executed</info>");
        return 0;
    }
}