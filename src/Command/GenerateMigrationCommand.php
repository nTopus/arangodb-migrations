<?php


namespace ArangoMigration\Command;


use ArangoMigration\Configuration\Configuration;
use ArangoMigration\Migration\GetterSetterMagicAttributes;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateMigrationCommand extends BaseCommand
{
    use GetterSetterMagicAttributes;

    protected static $defaultName = 'arangodb:migrations:generate';

    protected $up;

    protected $down;

    private static $template_filename = __DIR__.'/MigrationTemplate.txt';

    protected function configure()
    {
        $this->setName(self::$defaultName);
        $this->setDescription('Generate a blank migration class.');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \ArangoDBClient\Exception
     * @throws \TheSeer\Tokenizer\Exception
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $version = date('YmdHis');
        $path = $this->generateMigrationClass($this->configuration, $version);
        $output->writeln('Generate new migration class to '.$path);
        return 0;
    }

    protected function generateMigrationClass(Configuration $configuration, string $version): string
    {
        $placeHolders = array(
            '<namespace>',
            '<version>',
        );
        $replacements = array(
            $configuration->migrationsNamespace,
            $version
        );
        $template = file_get_contents(self::$template_filename);
        $code = str_replace($placeHolders, $replacements, $template);
        $directory = $this->getDirectory($configuration);
        if (!file_exists($directory)) {
            throw new \InvalidArgumentException("The migrations directory '{$directory}' does not exist.");
        }
        $path = $this->buildVersionPath($directory, $version);
        file_put_contents($path, $code);
        return $path;
    }

    protected function getDirectory(Configuration $configuration): string
    {
        $dir = $configuration->migrationDirectory ?: getcwd();
        return rtrim($dir, '/');
    }

    protected function buildVersionPath(string $dir, string $version): string
    {
        return "{$dir}/Version{$version}.php";
    }
}