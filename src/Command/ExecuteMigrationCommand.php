<?php


namespace ArangoMigration\Command;


use ArangoDBClient\Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExecuteMigrationCommand extends HandlerCommand
{
    protected static $defaultName = 'arangodb:migrations:execute';

    protected function configure()
    {
        $this->setName(self::$defaultName);
        $this->setDescription('Execute a single migration version up or down manually.');
        $this->addArgument(
            self::VERSION,
            InputArgument::REQUIRED,
            'The version to execute.',
            null
        );
        $this->addArgument(
            self::DIRECTION,
            InputArgument::OPTIONAL,
            'The migration direction to execute (up, down).',
            self::UP
        );
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \ArangoDBClient\Exception
     * @throws \TheSeer\Tokenizer\Exception
     * @throws \Exception
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $version = $input->getArgument(self::VERSION);
        $direction = (mb_strtolower($input->getArgument(self::DIRECTION)) !== self::UP) ? self::DOWN : self::UP;
        if (!$this->verifyMigrationNeedsBeMigrated($direction, $version, $output)) {
            return 0;
        }
        return $this->migrationHandler->execute($output, $version, $direction);
    }

    /**
     * @param string $direction
     * @param int $version
     * @param OutputInterface $output
     * @return bool
     * @throws Exception
     */
    protected function verifyMigrationNeedsBeMigrated(string $direction, int $version, OutputInterface $output): bool
    {
        if ($direction === self::UP && $this->migrationIsMigrated($version)) {
            $output->writeln("<info>This migration direction up already been executed.</info>");
            return false;
        }
        if ($direction === self::DOWN && !$this->migrationIsMigrated($version)) {
            $output->writeln("<info>This migration direction down already been executed.</info>");
            return false;
        }
        return true;
    }

    /**
     * @param int $version
     * @return bool
     * @throws Exception
     */
    protected function migrationIsMigrated(int $version): bool
    {
        $cursor = $this->migrationHandler->getMigrationDocumentByVersion($version);
        if ($cursor->getCount() == 0) {
            return false;
        }
        return true;
    }
}