<?php


namespace Tests\helper;


use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Connection;
use ArangoDBClient\Document;
use ArangoDBClient\Exception;
use GuzzleHttp\Client;

trait Helper
{
    private function clearLastMigrationFile()
    {
        $files = glob('tests/migration_arango/*');
        arsort($files);
        foreach ($files as $file) {
            if (
                strpos($file, 'tests/migration_arango/Version20200310200614.php') !== false ||
                strpos($file, 'tests/migration_arango/Version20200318034156.php') !== false ||
                strpos($file, 'tests/migration_arango/Version20200311005355.php') !== false
            ) {
                continue;
            }
            unlink($file);
            break;
        }
    }

    /**
     * @param Connection $connection
     * @throws Exception
     */
    private function createArangoDatabase(Connection $connection)
    {
        $client = new Client(['base_uri' => 'http://arango.svc:8529']);
        try {
            $client->get('/_db/dummy_db', [
                'auth' => [
                    'root', 'dummy_password'
                ]
            ]);
        } catch (\Throwable $e) {
            if ($e->getCode() == 404) {
                $response = $client->post('/_api/database',
                    [
                        'auth' => [
                            'root', 'dummy_password'
                        ],
                        'body' => '{"name":"dummy_db"}'
                    ]
                );
            }
        }
    }

    /**
     * @param CollectionHandler $collectionHandler
     * @throws Exception
     */
    private function dropAndCreateArangoMigrationCollection(CollectionHandler $collectionHandler)
    {
        if (!$collectionHandler->has('arango_migration_versions_test')) {
            $collectionHandler->create('arango_migration_versions_test');
        } else {
            $this->dropArangoCollection($collectionHandler, 'arango_migration_versions_test');
            $collectionHandler->create('arango_migration_versions_test');
        }
    }

    private function dropArangoDatabase()
    {
        $client = new Client(['base_uri' => 'http://arango.svc:8529']);
        $client->delete('/_api/database/dummy_db', [
            'auth' => [
                'root', 'dummy_password'
            ]
        ]);
    }

    /**
     * @param CollectionHandler $handler
     * @param $collection
     * @throws Exception
     */
    private function dropArangoCollection(CollectionHandler $handler, $collection)
    {
        if ($handler->has($collection)) {
            $handler->drop($collection);
        }
    }

    /**
     * @param CollectionHandler $collectionHandler
     * @param int $version
     * @return Document
     * @throws Exception
     */
    private function getMigrateDocumentByVersion(CollectionHandler $collectionHandler, int $version): Document
    {
        $cursor = $this->getMigrationCursorByVersion($collectionHandler, $version);
        return $cursor->getAll()[0];
    }

    /**
     * @param CollectionHandler $collectionHandler
     * @param int $version
     * @return \ArangoDBClient\cursor
     * @throws Exception
     */
    private function getMigrationCursorByVersion(CollectionHandler $collectionHandler, int $version)
    {
        return $collectionHandler->byExample('arango_migration_versions_test', ['v' => $version]);
    }

    /**
     * @param CollectionHandler $collectionHandler
     * @return \ArangoDBClient\cursor
     * @throws Exception
     */
    private function getAllMigrationDocumentsCursor(CollectionHandler $collectionHandler)
    {
        return $collectionHandler->byExample('arango_migration_versions_test', []);
    }
}