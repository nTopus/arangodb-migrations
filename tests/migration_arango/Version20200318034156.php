<?php

namespace Tests\migration_arango;

use ArangoDBClient\Collection;
use ArangoDBClient\Connection;
use ArangoMigration\Migration\ArangoDbMigration;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\DocumentHandler;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200318034156 implements ArangoDbMigration
{
    /**
     * @return string
     */
    public function getDescription()
    {
        return "";
    }

    public function up(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        $col = new Collection('test-2');
        $collectionHandler->create($col);
    }

    public function down(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        $collectionHandler->drop('test-2');
    }
}