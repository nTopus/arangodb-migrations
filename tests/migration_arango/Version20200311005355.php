<?php

namespace Tests\migration_arango;

use ArangoDBClient\Connection;
use ArangoDBClient\Document;
use ArangoMigration\Migration\ArangoDbMigration;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\DocumentHandler;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200311005355 implements ArangoDbMigration
{
    /**
     * @return string
     */
    public function getDescription()
    {
        return "";
    }

    public function up(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        $documentHandler->insert('test', ['name' => 'test2']);
    }

    public function down(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        $cursor = $collectionHandler->byExample('test', ['name'=> 'test2']);
        /** @var Document $doc */
        $doc = $cursor->getAll()[0];
        $documentHandler->removeById('test', $doc->getId());
    }
}