<?php

namespace Tests\migration_arango;

use ArangoDBClient\Collection;
use ArangoDBClient\Connection;
use ArangoDBClient\Document;
use ArangoMigration\Migration\ArangoDbMigration;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\DocumentHandler;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200310200614 implements ArangoDbMigration
{
    /**
     * @return string
     */
    public function getDescription()
    {
        return "";
    }

    /**
     * @param CollectionHandler $collectionHandler
     * @param DocumentHandler $documentHandler
     * @throws \ArangoDBClient\Exception
     */
    public function up(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        $col = new Collection('test');
        $collectionHandler->create($col);
        $doc = new Document();
        $doc->set('name', 'test');
        $documentHandler->insert('test', $doc);
    }

    public function down(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        $cursor = $collectionHandler->byExample('test', ['name'=> 'test']);
        /** @var Document $doc */
        $doc = $cursor->getAll()[0];
        $documentHandler->removeById('test', $doc->getId());
    }
}