<?php


namespace Tests;

use ArangoMigration\Command\GenerateMigrationCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Yaml\Yaml;
use Tests\helper\Helper;

class GenerateMigrationTest extends TestCase
{

    use Helper;
    private $application;

    protected function setUp(): void
    {
        $this->application = new Application();
        $this->application->add(new GenerateMigrationCommand());
        $this->clearLastMigrationFile();
        $this->undoConfigurationFile();
    }

    public function testNotGenerateMigrationWithoutConfigFileSetting()
    {
        try {
            $command = $this->application->find('arangodb:migrations:generate');
            $commandTester = new CommandTester($command);
            $commandTester->execute([]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('The configuration option does not exist.', $e->getMessage());
            $this->assertCount(3, glob('tests/migration_arango/*'));
        }
    }

    public function testNotGenerateMigrationWithIncorrectFileSetting()
    {
        $config = Yaml::parse(file_get_contents(__DIR__.'/config/configuration.yaml'));
        $config['configuration']['migrations_directory'] = 'test';
        $dump = Yaml::dump($config);
        file_put_contents(__DIR__ . '/config/configuration.yaml', $dump);
        try {
            $command = $this->application->find('arangodb:migrations:generate');
            $commandTester = new CommandTester($command);
            $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals("The migrations directory 'test' does not exist.", $e->getMessage());
            $this->assertCount(3, glob('tests/migration_arango/*'));
        }
    }

    public function testGenerateMultiplesMigrationTimestamp()
    {
        $command = $this->application->find('arangodb:migrations:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
        $output1 = $commandTester->getDisplay();
        preg_match('/([0-9]*)\.php/', $output1, $fistMigration);
        sleep(1);
        $app = new Application();
        $app->add(new GenerateMigrationCommand());
        $command = $this->application->find('arangodb:migrations:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
        $output2 = $commandTester->getDisplay();
        preg_match('/([0-9]*)\.php/', $output2, $secondMigration);
        sleep(1);
        $command = $this->application->find('arangodb:migrations:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--configuration' => 'tests/config/configuration.yaml',
        ]);
        $output3 = $commandTester->getDisplay();
        preg_match('/([0-9]*)\.php/', $output3, $thirdMigration);
        $this->assertGreaterThan($fistMigration[1], $secondMigration[1]);
        $this->assertGreaterThan($secondMigration[1], $thirdMigration[1]);
        $this->clearSpecificMigrationFile($fistMigration[1]);
        $this->clearSpecificMigrationFile($secondMigration[1]);
        $this->clearSpecificMigrationFile($thirdMigration[1]);
    }

    public function testGenerateMigrationFile()
    {
        $command = $this->application->find('arangodb:migrations:generate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
        $output = $commandTester->getDisplay();
        preg_match('/([0-9]*)\.php/', $output, $matches);
        $this->assertStringContainsString("Generate new migration class to tests/migration_arango/Version{$matches[1]}", $output);
        $this->assertFileExists("tests/migration_arango/Version{$matches[1]}.php");
        $template = file_get_contents(__DIR__ . '/template/_generate.txt');
        $placeHolders = array(
            '<namespace>',
            '<version>',
            '<up>',
            '<down>'
        );
        $replacements = array(
            'Tests\migration_arango',
            $matches[1]
        );
        $fileContent = str_replace($placeHolders, $replacements, $template);
        $this->assertEquals($fileContent, file_get_contents(__DIR__ . "/migration_arango/Version{$matches[1]}.php"));
    }

    private function clearSpecificMigrationFile(string $version)
    {
        $file = "tests/migration_arango/Version{$version}.php";
        if (file_exists($file)) {
            unlink($file);
        }
    }

    private function undoConfigurationFile()
    {
        $config = Yaml::parse(file_get_contents(__DIR__ . '/config/configuration.yaml'));
        $config['configuration']['migrations_directory'] = 'tests/migration_arango';
        $dump = Yaml::dump($config);
        file_put_contents(__DIR__ . '/config/configuration.yaml', $dump);
    }
}