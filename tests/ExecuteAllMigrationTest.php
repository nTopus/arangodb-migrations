<?php


namespace Tests;


use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Connection;
use ArangoDBClient\Cursor;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\ServerException;
use ArangoMigration\Command\ExecuteAllMigrationCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\helper\Helper;

class ExecuteAllMigrationTest extends TestCase
{
    use Helper;
    private $application;

    private $connection;

    private $handler;

    /**
     * @throws \ArangoDBClient\Exception
     */
    protected function setUp(): void
    {
        $this->application = new Application();
        $this->application->add(new ExecuteAllMigrationCommand());
        $this->connection = new Connection([
            'database' => 'dummy_db',
            'endpoint' => 'tcp://arango.svc:8529',
            'AuthUser' => 'root',
            'AuthPasswd' => 'dummy_password',
            'timeout' => 3,
            'Reconnect' => true,
            'createCollection' => true
        ]);
        $this->handler = new CollectionHandler($this->connection);
        $this->createArangoDatabase($this->connection);
        $this->dropArangoCollection($this->handler, 'test');
        $this->dropArangoCollection($this->handler, 'test-2');
        $this->clearLastMigrationFile();
        $this->dropAndCreateArangoMigrationCollection($this->handler);
    }


    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteMigrationWithoutSettingConfigFile()
    {
        try {
            $command = $this->application->find('arangodb:migrations:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute([]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('The configuration option does not exist.', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteMigrationWithIncorrectPathConfigFile()
    {
        try {
            $command = $this->application->find('arangodb:migrations:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute(['--configuration' => 'tests/config/']);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('Configuration file not setting.', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteMigrationWithIncorrectConfigFile()
    {
        try {
            $command = $this->application->find('arangodb:migrations:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200310200614',
                '--configuration' => 'tests/config/incorrect-configuration.yaml'
            ]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('Invalid configuration setting (Reason: The Migrations namespace is required, The Migrations directory is required)', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
        try {
            $command = $this->application->find('arangodb:migrations:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200310200614',
                '--configuration' => 'tests/config/incorrect-db-configuration.yaml'
            ]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('Invalid db-configuration setting (Reason: The Password is required, The User is required)', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
    }

    public function testExecuteMigrationStartingByVersion()
    {
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200311005355',
            '--configuration' => 'tests/config/configuration.yaml'
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Migrating up to 20200311005355 from 0', $output);
        $this->assertStringContainsString('++ migrating 20200310200614', $output);
        $this->assertStringContainsString('++ migrating 20200311005355', $output);
        $this->assertStringContainsString('2 Migrations executed', $output);
        $this->assertFalse($this->handler->has('test-2'));
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteDownToSpecificMigrationWhenMigrationSequenceIsBrokenOnDatabase()
    {
        $this->markMigrationAsMigrated(20200311005355);
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200311005355',
            '--configuration' => 'tests/config/configuration.yaml'
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Migrating down to 20200311005355 from 20200311005355', $output);
        $this->assertStringContainsString('Migration 20200311005355 skipped (Reason: AQL: collection or view not found: name: test (while parsing))', $output);
        $cursor = $this->handler->byExample('arango_migration_versions_test', ['v' => 20200311005355]);
        $this->assertInstanceOf(Cursor::class, $cursor);
        $this->assertCount(1, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testStopMigrationUpProcessWhenDatabaseNotExist()
    {
        try {
            $this->dropArangoDatabase();
            $command = $this->application->find('arangodb:migrations:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200311005355',
                '--configuration' => 'tests/config/configuration.yaml'
            ]);
            $this->fail("Do not generate throw");
        } catch (\Throwable $e) {
            $this->assertEquals(get_class($e), ServerException::class);
            $this->assertEquals('database not found', $e->getMessage());
        }
    }

    public function testAskConfirmationWhenMigrationFileIsMissing()
    {
        $this->markMigrationAsMigrated(202003110053875);
        $this->markMigrationAsMigrated(202003110053876);
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['no']);
        $commandTester->execute([ '--configuration' => 'tests/config/configuration.yaml']);
        $commandTester->setInputs(['no']);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('WARNING! You have 2 previously executed migrations in the database that are not registered migrations.', $output);
        $this->assertStringContainsString('Migration cancelled!', $output);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testExecuteAllAvailableMigrationDownStartingFromAVersion()
    {
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
        $commandTester->execute([
            'version' => '20200311005355',
            '--configuration' => 'tests/config/configuration.yaml'
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Migrating down to 20200311005355 from 20200318034156', $output);
        $this->assertStringContainsString('-- migrating 20200318034156', $output);
        $this->assertStringContainsString('-- migrating 20200311005355', $output);
        $this->assertStringContainsString('2 Migrations executed', $output);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200318034156);
        $this->assertCount(0, $cursor);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200311005355);
        $this->assertCount(0, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testExecuteAllMigrationUpAvailable()
    {
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Migrating up to 20200318034156 from 0', $output);
        $this->assertStringContainsString('++ migrating 20200310200614', $output);
        $this->assertStringContainsString('++ migrating 20200311005355', $output);
        $this->assertStringContainsString('++ migrating 20200318034156', $output);
        $this->assertStringContainsString('3 Migrations executed', $output);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200310200614);
        $this->assertCount(1, $cursor);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200311005355);
        $this->assertCount(1, $cursor);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200318034156);
        $this->assertCount(1, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testExecuteAllMigrationUpAvailableStartingFromVersion()
    {
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200311005355',
            '--configuration' => 'tests/config/configuration.yaml'
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Migrating up to 20200311005355 from 0', $output);
        $this->assertStringContainsString('++ migrating 20200311005355', $output);
        $this->assertStringContainsString('++ migrating 20200310200614', $output);
        $this->assertStringContainsString('2 Migrations executed', $output);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200310200614);
        $this->assertCount(1, $cursor);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200311005355);
        $this->assertCount(1, $cursor);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200318034156);
        $this->assertCount(0, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testExecuteOnlyLastMigrationDownAvailable()
    {
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([ '--configuration' => 'tests/config/configuration.yaml']);
        $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Migrating down to 20200318034156 from 20200318034156', $output);
        $this->assertStringContainsString('-- migrating 20200318034156', $output);
        $cursor = $this->getMigrationCursorByVersion($this->handler, 20200318034156);
        $this->assertCount(0, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteAnyMigrationBecauseDirectoryIsEmpty()
    {
        $command = $this->application->find('arangodb:migrations:migrate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--configuration' => 'tests/config/configuration-empty-directory.yaml']);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Could not find any migrations to execute', $output);
        $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
        $this->assertCount(0, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteAvailableMigrationBecausePatternFileIsIncorrect()
    {
        try {
            $command = $this->application->find('arangodb:migrations:migrate');
            $commandTester = new CommandTester($command);
            $commandTester->execute(['--configuration' => 'tests/config/configuration-incorrect-pattern.yaml']);
            $this->fail("Do not generate throw");
        } catch (\Throwable $e) {
            $this->assertEquals(get_class($e), \UnexpectedValueException::class);
            $this->assertEquals('Migrate failed (Reason: Fix file name - `Version-20200310200614TEST.php` `Version20200310200614-TEST.php`)', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
    }

    private function markMigrationAsMigrated($version)
    {
        $doc = new Document();
        $doc->set('t', date('c'));;
        $doc->set('v', $version);
        $handler = new DocumentHandler($this->connection);
        $handler->insert('arango_migration_versions_test', $doc);
    }

    private function getInputStream($input)
    {
        $stream = fopen('php://memory', 'r+', false);
        fputs($stream, $input);
        rewind($stream);
        return $stream;
    }
}
