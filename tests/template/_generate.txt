<?php

namespace <namespace>;

use ArangoDBClient\Connection;
use ArangoMigration\Migration\ArangoDbMigration;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\DocumentHandler;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version<version> implements ArangoDbMigration
{
    /**
     * @return string
     */
    public function getDescription()
    {
        return "";
    }

    public function up(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Connection $connection, CollectionHandler $collectionHandler, DocumentHandler $documentHandler)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}