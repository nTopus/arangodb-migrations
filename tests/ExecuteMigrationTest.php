<?php


namespace Tests;


use ArangoDBClient\Collection;
use ArangoDBClient\CollectionHandler;
use ArangoDBClient\Connection;
use ArangoDBClient\Cursor;
use ArangoDBClient\Document;
use ArangoDBClient\DocumentHandler;
use ArangoDBClient\Exception;
use ArangoDBClient\ServerException;
use ArangoMigration\Command\ExecuteMigrationCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\helper\Helper;

class ExecuteMigrationTest extends TestCase
{
    use Helper;
    private $application;

    private $connection;

    private $handler;

    /**
     * @throws \ArangoDBClient\Exception
     */
    protected function setUp(): void
    {
        $this->application = new Application();
        $this->application->add(new ExecuteMigrationCommand());
        $this->connection = new Connection([
            'database' => 'dummy_db',
            'endpoint' => 'tcp://arango.svc:8529',
            'AuthUser' => 'root',
            'AuthPasswd' => 'dummy_password',
            'timeout' => 3,
            'Reconnect' => true,
            'createCollection' => true
        ]);
        $this->handler = new CollectionHandler($this->connection);
        $this->createArangoDatabase($this->connection);
        $this->dropAndCreateArangoMigrationCollection($this->handler);
        $this->dropArangoCollection($this->handler, 'test');
    }

    public function testCantExecuteMigrateUpExecuted()
    {
        $documentHandler = new DocumentHandler($this->connection);
        $document = new Document();
        $document->set('v', 20200310200614);
        $document->set('t', date('c'));
        $documentHandler->insert('arango_migration_versions_test', $document);
        $command = $this->application->find('arangodb:migrations:execute');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200310200614',
            '--configuration' => 'tests/config/configuration.yaml',
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('This migration direction up already been executed.', $output);
    }

    public function testCantExecuteMigrateDownExecuted()
    {
        $command = $this->application->find('arangodb:migrations:execute');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200310200614',
            'direction' => 'down',
            '--configuration' => 'tests/config/configuration.yaml',
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('This migration direction down already been executed.', $output);
    }

    public function testCantExecuteMigrationWithoutSettingVersion()
    {
        try {
            $command = $this->application->find('arangodb:migrations:execute');
            $commandTester = new CommandTester($command);
            $commandTester->execute(['--configuration' => 'tests/config/configuration.yaml']);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), RuntimeException::class);
            $this->assertEquals('Not enough arguments (missing: "version").', $e->getMessage());
        }
    }

    public function testCantExecuteMigrationWithoutSettingConfigFile()
    {
        try {
            $command = $this->application->find('arangodb:migrations:execute');
            $commandTester = new CommandTester($command);
            $commandTester->execute([]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('The configuration option does not exist.', $e->getMessage());
        }
    }

    public function testCantExecuteMigrationWithIncorrectPathConfigFile()
    {
        try {
            $command = $this->application->find('arangodb:migrations:execute');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200310200614',
                '--configuration' => 'tests/config/'
            ]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('Configuration file not setting.', $e->getMessage());
        }
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteMigrationWithIncorrectConfigFile()
    {
        try {
            $command = $this->application->find('arangodb:migrations:execute');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200310200614',
                '--configuration' => 'tests/config/incorrect-configuration.yaml'
            ]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('Invalid configuration setting (Reason: The Migrations namespace is required, The Migrations directory is required)', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
        try {
            $command = $this->application->find('arangodb:migrations:execute');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200310200614',
                '--configuration' => 'tests/config/incorrect-db-configuration.yaml'
            ]);
            $this->fail("Do not generate throw");
        } catch (\Exception $e) {
            $this->assertEquals(get_class($e), \InvalidArgumentException::class);
            $this->assertEquals('Invalid db-configuration setting (Reason: The Password is required, The User is required)', $e->getMessage());
            $cursor = $this->getAllMigrationDocumentsCursor($this->handler);
            $this->assertCount(0, $cursor);
        }
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testCantExecuteMigrationWhenDatabaseDoesNotExist()
    {
        try {
            $this->dropArangoDatabase();
            $command = $this->application->find('arangodb:migrations:execute');
            $commandTester = new CommandTester($command);
            $commandTester->execute([
                'version' => '20200310200614',
                '--configuration' => 'tests/config/configuration.yaml',
            ]);
            $this->fail("Do not generate throw");
        } catch (\Throwable $e) {
            $this->assertEquals(get_class($e), ServerException::class);
            $this->assertEquals('database not found', $e->getMessage());
        }
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testExecuteUpMigration()
    {
        $command = $this->application->find('arangodb:migrations:execute');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200310200614',
            '--configuration' => 'tests/config/configuration.yaml',
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('++ migrating 20200310200614', $output);
        /** @var Document $document */
        $this->assertInstanceOf(Document::class, $this->getMigrateDocumentByVersion($this->handler, '20200310200614'));
        $this->assertInstanceOf(Collection::class, $this->handler->get('test'));
        $cursor = $this->handler->byExample('test', ['name' => 'test']);
        $this->assertInstanceOf(Cursor::class, $cursor);
        $this->assertCount(1, $cursor);
    }

    /**
     * @throws \ArangoDBClient\Exception
     */
    public function testExecuteDownMigration()
    {
        $command = $this->application->find('arangodb:migrations:execute');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'version' => '20200310200614',
            '--configuration' => 'tests/config/configuration.yaml',
        ]);
        $commandTester->execute([
            'version' => '20200310200614',
            'direction' => 'down',
            '--configuration' => 'tests/config/configuration.yaml',
        ]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('-- migrating 20200310200614', $output);
        $cursor = $this->handler->byExample('arango_migration_versions_test', ['v' => '20200310200614']);
        $this->assertInstanceOf(Cursor::class, $cursor);
        $this->assertCount(0, $cursor);
        $cursor = $this->handler->byExample('test', ['name' => 'test']);
        $this->assertInstanceOf(Cursor::class, $cursor);
        $this->assertCount(0, $cursor);
    }
}